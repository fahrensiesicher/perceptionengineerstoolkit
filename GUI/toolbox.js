Blockly.Blocks['persistence'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Import","read"], ["Export","write"]]), "action");
    this.appendDummyInput()
        .appendField("Plugin")
        .appendField(new Blockly.FieldTextInput(""), "plugin");
    this.appendDummyInput()
        .appendField("Parameters")
        .appendField(new Blockly.FieldTextInput(""), "parameters");
    this.setNextStatement(true, "DataModel");
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['preprocess'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Preprocess");
    this.appendDummyInput()
        .appendField("Plugin")
        .appendField(new Blockly.FieldTextInput(""), "plugin");
    this.appendDummyInput()
        .appendField("Parameters")
        .appendField(new Blockly.FieldTextInput(""), "parameters");
    this.setPreviousStatement(true, "DataModel");
    this.setNextStatement(true, "DataModel");
    this.setColour(120);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};