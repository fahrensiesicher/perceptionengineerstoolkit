Blockly.Python['persistence'] = function(block) {
  var dropdown_action = block.getFieldValue('action');
  var text_plugin = block.getFieldValue('plugin');
  var text_parameters = block.getFieldValue('parameters');
  var code = '- {plugin: "'+text_plugin+'", action:"'+dropdown_action+'", parameters: {text_parameters}}\n';
  return code;
};

Blockly.Python['preprocess'] = function(block) {
  var text_plugin = block.getFieldValue('plugin');
  var text_parameters = block.getFieldValue('parameters');
  var code = '- {plugin: "'+text_plugin+'", parameters: {text_parameters}}\n';
  return code;
};
