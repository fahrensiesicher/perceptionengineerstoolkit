from itertools import count

from PerceptionToolkit.CommandProcessor import *
from yapsy.PluginManager import PluginManager
from typing import Dict
import glob
import os


def add_command(controller: CommandProcessor, plugin_manager: PluginManager, plugin_name: str, plugin_action: str,
                plugin_parameters: Dict):
    plugin = CommandProcessor.find_plugin(plugin_manager, plugin_name)
    controller.append_command(Command(plugin, plugin_action, plugin_parameters))


# This function initializes the script.
def run(plugin_manager: PluginManager) -> None:
    print("Executing Yarbus.py to analyze Yarbus data.")

    # Usage of the CommandProcessor is completely optional, you can call plugin function directly as well.
    # However, it is very convenient.
    controller = CommandProcessor()

    #experiment = "DefendingYarbus"     # 14 % baseline
    experiment = "ReconsideringYarbus"  # 25 % baseline

    #step1(controller, plugin_manager, experiment)
    #controller.reset()  # Controller needs to be reset because we load the cached data from step 1.
    step2(controller, plugin_manager, experiment)


def step1(controller: CommandProcessor, plugin_manager: PluginManager, experiment: str) -> None:
    load_file = lambda current_file: add_command(controller, plugin_manager, "PersistenceCSV", "read",
                                                 {"filename": current_file, "separator": "\t", "skip_header_lines": 0,
                                                  "comment_symbol": "#", "trial_split_symbol": "",
                                                  "invalid_value": -9999,  # this enables checking for NaN
                                                  "aliases": {"TIME": "Time", "RIGHT_EYE_X": "Gaze point right X",
                                                              "RIGHT_EYE_Y": "Gaze point right Y"}})

    files = glob.glob("test/data/Yarbus/" + experiment + "/*.tsv")
    for file in files:
        load_file(file)
    controller.execute()

    # set label , subject and stimulus metadata according to filenames
    for i, filepath in enumerate(files):
        filename = os.path.basename(filepath)
        controller.model[i].meta[DataModel.META.LABEL] = filename.split("_")[0][-1]
        controller.model[i].meta[DataModel.META.STIMULUS] = filename.split("_")[1][8:]
        controller.model[i].meta[DataModel.META.SUBJECT] = os.path.splitext(filename.split("_")[2])[0][3:]
        controller.model[i].meta["filename"] = filename

    # Exclude those weird recordings with just 250Hz sampling rate that should not be there.
    if experiment == "ReconsideringYarbus":
        add_command(controller, plugin_manager, "TrialFilterProperty", "", {"minimum_sampling_frequency": 1000.0,
                                                                            "minimum_duration": 60 * 1000})
    if experiment == "DefendingYarbus":
        add_command(controller, plugin_manager, "TrialFilterProperty", "", {"minimum_sampling_frequency": 1000.0,
                                                                            "minimum_duration": 30 * 1000})
    # Exclude all data with a bad sampling rate
    add_command(controller, plugin_manager, "TrialFilterQuality", "", {"min_tracking_ratio": 0.85})

    # Preprocess data
    add_command(controller, plugin_manager, "PreprocessGapFill", "", {"max_gap_length": 70})
    add_command(controller, plugin_manager, "PreprocessMedianFilter", "", {"time_window": 30})

    add_command(controller, plugin_manager, "PreprocessResample", "", {"target_sampling_rate": 120})

    # detect fixations (the experimental setup was different for the two experiments, so we need different calls)

    if experiment == "ReconsideringYarbus":
        add_command(controller, plugin_manager, "EventdetectionIVT", "", {"distance_eye_to_stimulus": 574,
                                                                          "stimulus_pixel_size": 0.3050,
                                                                          "merge_adjacent_fixations": True})
    if experiment == "DefendingYarbus":
        add_command(controller, plugin_manager, "EventdetectionIVT", "", {"distance_eye_to_stimulus": 1300,
                                                                          "stimulus_pixel_size": 0.5334,
                                                                          "merge_adjacent_fixations": True})

    controller.execute()

    # cache event detection
    target_files = []
    for d in controller.model:
        target_files.append("test/data/Yarbus/" + experiment + "/cache/" + os.path.basename(d.meta["filename"]))

    add_command(controller, plugin_manager, "PersistenceCSV", "write",
                {"target_files": target_files, "separator": "\t"})
    controller.execute()


def step2(controller: CommandProcessor, plugin_manager: PluginManager, experiment: str) -> None:
    # load cached event detection data
    load_file = lambda current_file: add_command(controller, plugin_manager, "PersistenceCSV", "read",
                                                 {"filename": current_file, "separator": "\t", "skip_header_lines": 0,
                                                  "comment_symbol": "#", "trial_split_symbol": "",
                                                  "invalid_value": -9999,  # this enables checking for NaN
                                                  "aliases": {"TIME": "Time",
                                                              "RIGHT_EYE_X": "Gaze point right X",
                                                              "RIGHT_EYE_Y": "Gaze point right Y",
                                                              "RIGHT_EYE_VALID": "right eye valid",
                                                              "EVENT_TYPE": "event_type",
                                                              "EVENT_IDX": "event_idx"}})

    files = glob.glob("test/data/Yarbus/" + experiment + "/cache/*.tsv")

    #files = files[1:20]

    for file in files:
        load_file(file)
    controller.execute()

    # set label , subject and stimulus metadata according to filenames
    for i, filepath in enumerate(files):
        filename = os.path.basename(filepath)
        controller.model[i].meta[DataModel.META.LABEL] = filename.split("_")[0][-1]
        controller.model[i].meta[DataModel.META.STIMULUS] = filename.split("_")[1][8:]
        controller.model[i].meta[DataModel.META.SUBJECT] = os.path.splitext(filename.split("_")[2])[0][3:]

    add_command(controller, plugin_manager, "ClassificationEvolutionaryPattern", "fit", {})
    controller.execute()
